var http = require('request');
var cors = require('cors');
var uuid = require('uuid');
var url = require('url');
var util = require('util');
var moment = require('moment');
var fs = require('fs');
var _ = require('lodash');
var getUrls = require('get-urls');
var htmlToText = require('html-to-text');
var Confluence = require("confluence-api");

var getBaseUrl = function(url) {
  return url
    // strip known confluence endpoints
    .replace(/\/(display|pages|x|)\/.*/,'')
    // Strip trailing /
    .replace(/\/+$/,'');
};

var formatContent = function(html) {
  return htmlToText.fromString(html, {ignoreHref:true, ignoreImage:true});
}

var getConfluenceDetails = function(settings,page,callback) {
  // Link format: 
  // https://confluence.atlassian.com/display/DOC/Working+with+Links?focusedCommentId=368640803#comment-368640803
  // ID format: 
  // https://rndwww.nce.amadeus.net/confluence/pages/viewpage.action?pageId=154924885
  // We need to handle both separately...

  // Strip base url...
  var url = page.replace()


  fs.readFile(__dirname + '/../sample-confluence.json','utf8',(err,result) => {
    console.log(err);
    callback(JSON.parse(result));
  });
};


// This is the heart of your HipChat Connect add-on. For more information,
// take a look at https://developer.atlassian.com/hipchat/tutorials/getting-started-with-atlassian-connect-express-node-js
module.exports = function (app, addon) {
  var hipchat = require('../lib/hipchat')(addon);
  var contextPath = url.parse(addon.config.localBaseUrl()).path.replace(/\/$/,'');

  // simple healthcheck
  app.get(contextPath+'/healthcheck', function (req, res) {
    res.send('OK');
  });

  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get(contextPath+'/',
    function (req, res) {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
          res.render('homepage', addon.descriptor);
        },
        'application/json': function () {
          res.redirect('/atlassian-connect.json');
        }
      });
    }
  );

  // This is an example route that's used by the default for the configuration page
  // https://developer.atlassian.com/hipchat/guide/configuration-page
  app.get(contextPath+'/config',
    // Authenticates the request using the JWT token in the request
    addon.authenticate(),
    function (req, res) {
      addon.settings.get('roomSettings',req.clientInfo.clientKey)
        .then(settings => {
          res.render('config', settings);
        });
    }
  );

  // This is an example glance that shows in the sidebar
  // https://developer.atlassian.com/hipchat/guide/glances
  app.get(contextPath+'/glance',
    cors(),
    addon.authenticate(),
    function (req, res) {
      res.json({
        "label": {
          "type": "html",
          "value": "Confluence"
        }
      });
    }
  );

  // This is an example sidebar controller that can be launched when clicking on the glance.
  // https://developer.atlassian.com/hipchat/guide/sidebar
  app.get('/sidebar',
    addon.authenticate(),
    function (req, res) {
      res.render('sidebar', {
        identity: req.identity
      });
    }
  );

  // This is an example route to handle an incoming webhook
  // https://developer.atlassian.com/hipchat/guide/webhooks
  app.post('/webhook',
    addon.authenticate(),
    function (req, res) {
      // Return immediately
      res.json({ status: "ok" });

      // Iterate through all urls matching the configured confluence location
      var message = _.get(req.body,'item.message.message') || '';
      var urls = Array.from(getUrls(message,{stripFragment:false,stripWWW:false,removeQueryParameters:[],removeTrailingSlash: false}));

      // Only fetch settings if we have one or more urls
      // console.log(util.inspect(urls));
      if(urls.length) {
        addon.settings.get('roomSettings',req.clientInfo.clientKey)
        .then(settings => {
          // console.log(util.inspect(settings));
          // var confluence = new Confluence({
          //   username: settings.user,
          //   password: settings.pass,
          //   baseUrl:  settings.host,
          // });

          // Filter those matching the Confluence base url
          _.filter(urls, url => _.startsWith(url,settings.host))
            .forEach(url => {
              // console.log(`Fetching details for ${url}`);
              // Get details from the Confluence API

              getConfluenceDetails(settings,url,pageDescription => {
              // If exists, post a card
              if(pageDescription && pageDescription.title) {
                // Init card

                var title = _.get(pageDescription,'title');
                var content = _.get(pageDescription,'body.view.value');
                var lastUdatedBy = _.get(pageDescription,'version.by.displayName');
                var lastUdatedAt = moment(_.get(pageDescription,'version.when'));
                var spaceKey = _.get(pageDescription,'space.key');
                var spaceName = _.get(pageDescription,'space.name');

                var card = {
                  "style": "application",
                  "format": "medium",
                  "url": url,
                  "id": uuid.v4(),
                  "title": pageDescription.title,
                  "description": _.truncate(formatContent(content), {length: 150}),
                  "icon": {
                    "url": "https://hipchat-public-m5.atlassian.com/assets/img/hipchat/bookmark-icons/favicon-192x192.png"
                  },
                  "attributes": [
                    {
                      "label": "Updated by",
                      "value": {
                        "label": lastUdatedBy
                      }
                    },
                    {
                      "label": "At",
                      "value": {
                        "label": lastUdatedAt.fromNow()
                      }
                    },
                    {
                      "label": "Space",
                      "value": {
                        "label": `${spaceKey} - ${spaceName}`
                      }
                    },
                  ]
                };
                var msg = '<b>' + card.title + '</b>: ' + card.description;
                var opts = { 'options': { 'color': 'yellow' } };
                hipchat.sendMessage(req.clientInfo, req.identity.roomId, msg, opts, card);
              }
            });
        });
      });
    }
  });

  app.post('/configure',
    addon.authenticate(),
    function (req, res) {
      var settings = {
        host: getBaseUrl(_.get(req.body,'host')) || '',
        user: _.get(req.body,'user') || '',
        pass: _.get(req.body,'pass') || '',
      };

      addon.settings.set('roomSettings',settings,req.clientInfo.clientKey)
        .then(result => {
          res.sendStatus(200);
        })
        .catch(error => {
          res.status(500).send(`Error saving configuration: ${error}`);
        });
    }
  );

  // Notify the room that the add-on was installed. To learn more about
  // Connect's install flow, check out:
  // https://developer.atlassian.com/hipchat/guide/installation-flow
  addon.on('installed', function (clientKey, clientInfo, req) {
    hipchat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room');
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', function (id) {
    addon.settings.client.keys(id + ':*', function (err, rep) {
      rep.forEach(function (k) {
        addon.logger.info('Removing key:', k);
        addon.settings.client.del(k);
      });
    });
  });

};


